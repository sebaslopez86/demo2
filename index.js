// import express 
const express=require("express");
// creamos la aplicacion express
const app=express();

app.use(express.static("public"));

//iniciar app escuchando puerto parametro
app.listen(3000,() => (
    console.log("Servidor corriendo en el puerto 3000")
));